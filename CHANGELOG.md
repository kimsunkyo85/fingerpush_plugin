## 1.0.0
* TODO: Describe initial release.

## 1.0.1
* Android SDK 3.5.2 로 변경

## 1.0.2
* Android SDK 3.5.3 로 변경

## 1.0.3
* iOS PushListPage 추가

## 1.0.4
* iOS SDK 3.5.5 로 변경

## 1.0.5
* Android SDK 3.5.4 로 변경

## 1.0.6
* fluttter 2.0 오류 대응

## 1.0.7
*iOS 태그리스트가 없을 때 오류 수정
*iOS xcframework 로 변경
*setDevice,getPlatformVersion return "" 로 변경

## 1.0.8
*Android build.gradle 수정
*iOS willPresentNotificationHandler 추가

## 1.0.9
*iOS SDK 3.6.3 로 변경

## 1.0.10
*Android setAppSecret 오류 수정

## 1.0.11
*iOS willPresentNotificationHandler 를 구현하지 않은 경우 처리 추가

## 1.0.12
*Android Android 12 PendingIntent Flag 추가

## 1.1.0
*null safety 대응
*Android fingerpush_3.7.1.aar 적용, 인앱푸시 추가, getToken 추가
*iOS finger.xcframework 3.7.0 버전 적용, 인앱푸시(showInAppPush,setInAppPushHandler) 추가, getToken 추가

## 1.1.1
*iOS finger.xcframework 3.7.1 버전 적용, 릴리즈에서 푸시로 앱 실행시 오류 수정 
