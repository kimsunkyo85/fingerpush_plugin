#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint fingerpush_plugin.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'fingerpush_plugin'
  s.version          = '1.1.1'
  s.summary          = 'FingerPush Flutter plugin.'
  s.description      = <<-DESC
fingerpush Flutter plugin.
                       DESC
  s.homepage         = 'https://www.fingerpush.com/'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'KISSOFT' => 'partner@kissoft.co.kr' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.platform = :ios, '10.0'
  s.vendored_frameworks = "finger.xcframework"

  # Flutter.framework does not contain a i386 slice. Only x86_64 simulators are supported.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }

end
