#import <Flutter/Flutter.h>
#import <finger/finger.h>

@interface FingerPushPlugin : NSObject<FlutterPlugin>

+ (instancetype)sharedInstance;

@end
