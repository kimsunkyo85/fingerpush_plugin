#import "FingerPushPlugin.h"
#import <UserNotifications/UserNotifications.h>

#ifdef DEBUG
#define FPLog( s, ... ) NSLog( @"%s \n%@", __func__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define FPLog( s, ... )
#endif

@interface FingerPushPlugin ()<FlutterApplicationLifeCycleDelegate>

@property (strong, nonatomic) FlutterMethodChannel *channel;
@property (strong, nonatomic) finger *fingerManager;
@property int countTryCallWillPresent;

@end

@implementation FingerPushPlugin

+ (instancetype)sharedInstance {
    static FingerPushPlugin *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [FingerPushPlugin new];
        sharedInstance.countTryCallWillPresent = 0;
    });
    return sharedInstance;
}

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {

    FingerPushPlugin.sharedInstance.fingerManager = [finger sharedData];
    FPLog(@"fingerpush framework version : %@",[finger getSdkVer]);

    FingerPushPlugin.sharedInstance.channel = [FlutterMethodChannel methodChannelWithName:@"fingerpush_plugin" binaryMessenger:[registrar messenger]];
    [registrar addMethodCallDelegate:FingerPushPlugin.sharedInstance channel:FingerPushPlugin.sharedInstance.channel];
//    [registrar addApplicationDelegate:FingerPushPlugin.sharedInstance];
    //
    FlutterAppDelegate *delegate = (FlutterAppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate addApplicationLifeCycleDelegate:FingerPushPlugin.sharedInstance];
    
}

- (void)handleMethodCall:(FlutterMethodCall *)call result:(FlutterResult)result {
    
    FPLog(@"call.method : %@",call.method);
    FPLog(@"call.arguments : %@",call.arguments);
    
    if ([@"FingerPush#setAppKey" isEqualToString:call.method]) {
        
        [self.fingerManager setAppKey:call.arguments];
        result(nil);
        
    } else if ([@"FingerPush#setAppSecret" isEqualToString:call.method]) {
        
        [self.fingerManager setAppScrete:call.arguments];
        result(nil);
        
    } else if ([@"FingerPush#setDevice" isEqualToString:call.method]) {
        
        [self registeredForRemoteNotifications:[UIApplication sharedApplication]];
        result(nil);
        
    } else if ([@"FingerPush#setEnable" isEqualToString:call.method]) {
        
        BOOL isEnable = [call.arguments boolValue];
        
        [self.fingerManager setEnable:isEnable :^(NSString *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result(posts);
        }];
        
    } else if ([@"FingerPush#setAdPushEnable" isEqualToString:call.method]) {
        
        BOOL isEnable = [call.arguments boolValue];
        
        [self.fingerManager requestSetAdPushEnable:isEnable :^(NSString *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result(posts);
        }];
        
    } else if ([@"FingerPush#requestPushContent" isEqualToString:call.method]) {
        
       [self.fingerManager requestPushContentWithBlock:call.arguments :^(NSDictionary *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
           result([self convertedFromObjToSting:posts]);
       }];
        
//        *  @param strMsgTag  메세지 tag
//        *  @param strMsgMode 메세지 mode
        // NSString *strMsgTag = call.arguments[@"MsgTag"];
        // NSString *strMsgMode = call.arguments[@"MsgMode"];
        
        // [self.fingerManager requestPushContentWithBlock:strMsgTag :strMsgMode :^(NSDictionary *posts, NSError *error) {
        //     FPLog(@"post : %@ / error : %@",posts,error);
        //     if (error) {
        //         FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
        //         result(flutterError);
        //         return;
        //     }
        //     result(@{@"result":posts});
        // }];
        
    } else if ([@"FingerPush#requestPushInfo" isEqualToString:call.method]) {

        [self.fingerManager requestPushInfoWithBlock:^(NSDictionary *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result([self convertedFromObjToSting:posts]);
        }];
        
    } else if ([@"FingerPush#requestRegTag" isEqualToString:call.method]) {
        
        NSArray *arrTag = nil;
        
        if ([[call arguments] isKindOfClass:[NSArray class]]) {
            arrTag = [NSArray arrayWithArray:[call arguments]];
        }

        [self.fingerManager requestRegTagWithBlock:arrTag :^(NSString *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result(posts);
        }];
        
    } else if ([@"FingerPush#requestRemoveTag" isEqualToString:call.method]) {

        NSArray *arrTag = nil;
        
        if ([[call arguments] isKindOfClass:[NSArray class]]) {
            arrTag = [call arguments];
        }

        [self.fingerManager requestRemoveTagWithBlock:arrTag :^(NSString *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result(posts);
        }];
        
    } else if ([@"FingerPush#requestRemoveAllTag" isEqualToString:call.method]) {

        [self.fingerManager requestRemoveAllTagWithBlock:^(NSString *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result(posts);
        }];
        
    } else if ([@"FingerPush#requestGetDeviceTagList" isEqualToString:call.method]) {
        
        [self.fingerManager requestGetDeviceTagListWithBlock:^(NSArray *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result([self convertedFromObjToSting:posts]);
        }];
        
    } else if ([@"FingerPush#requestGetAllTagList" isEqualToString:call.method]) {

        [self.fingerManager requestGetAllTagListWithBlock:^(NSArray *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result([self convertedFromObjToSting:posts]);
        }];
        
    } else if ([@"FingerPush#requestRegId" isEqualToString:call.method]) {
        
        NSString *strId = call.arguments;

        [self.fingerManager requestRegIdWithBlock:strId :^(NSString *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result(posts);
        }];
        
    } else if ([@"FingerPush#requestRemoveId" isEqualToString:call.method]) {

        [self.fingerManager requestRemoveIdWithBlock:^(NSString *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result(posts);
        }];
        
    } else if ([@"FingerPush#requestRegUniqId" isEqualToString:call.method]) {
        
        NSString *strUniqId = call.arguments[@"UniqId"];
        BOOL isArlram = [call.arguments[@"IsAlram"] boolValue];
        NSString *strMsg = call.arguments[@"Msg"];

        [self.fingerManager requestRegUniqIdWithBlock:strUniqId isAlram:isArlram msg:strMsg :^(NSString *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result(posts);
        }];
        
    } else if ([@"FingerPush#requestGetAppReport" isEqualToString:call.method]) {

        [self.fingerManager requestGetAppReportWithBlock:^(NSDictionary *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
                        
            result([self convertedFromObjToSting:posts]);
        }];
        
    } else if ([@"FingerPush#requestPushCheck" isEqualToString:call.method]) {

        [self.fingerManager requestPushCheckWithBlock:call.arguments :^(NSString *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result(posts);
        }];
        
    } else if ([@"FingerPush#requestPushList" isEqualToString:call.method]) {
                
        [self.fingerManager requestPushListWithBlock:^(NSArray *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result([self convertedFromObjToSting:posts]);
        }];
        
    } else if ([@"FingerPush#requestPushListPage" isEqualToString:call.method]) {
        
        int intPage = [call.arguments[@"page"] intValue];
        int intCount = [call.arguments[@"listcnt"] intValue];
        
        [self.fingerManager requestPushListPageWithBlock:intPage Cnt:intCount :^(NSDictionary *posts, NSError *error) {
            FPLog(@"post : %@ / error : %@",posts,error);
            if (error) {
                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
                result(flutterError);
                return;
            }
            result([self convertedFromObjToSting:posts]);
        }];
        
    } else if ([@"FingerPush#showInAppPush" isEqualToString:call.method]) {
        
        [self.fingerManager showInAppPush:nil closeEvent:^(NSDictionary *msg, FINGER_IN_APP_CLOSED_EVENT event, NSError *error) {

            FPLog(@"popupInAppPush ====> \n%@ \n %u %@ ",msg, event,error);
                                    
            if(event==kInAppOnClick) {
                // 클릭시
                NSDictionary *dic = @{@"event":@"Click",@"data":msg};
                [self.channel invokeMethod:@"FingerPush#setInAppPushHandler" arguments:dic];
            }else if (event==kInAppNotToday) {
                // 하루동안 보지않기
                NSDictionary *dic = @{@"event":@"NotToday",@"data":msg};
                [self.channel invokeMethod:@"FingerPush#setInAppPushHandler" arguments:dic];
            }else if (event==kInAppClose) {
                // 닫기
                NSDictionary *dic = @{@"event":@"Close",@"data":msg};
                [self.channel invokeMethod:@"FingerPush#setInAppPushHandler" arguments:dic];
            }else if (event==kInAppFail) {
                //인앱메세지 불러오기 실패
//                [self.channel invokeMethod:@"FingerPush#setInAppPushHandler" arguments:@"Fail"];
//                FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
//                result(flutterError);
//                [self.channel invokeMethod:@"FingerPush#setInAppPushHandler" arguments:dic];
//                [self.channel invokeMethod:@"FingerPush#setDeviceHandler" arguments:error.description];
                NSDictionary *dic = @{@"event":@"Fail",@"data":error.description};
                [self.channel invokeMethod:@"FingerPush#setInAppPushHandler" arguments:dic];
            }
            
        }];
        
    } else if ([@"FingerPush#getToken" isEqualToString:call.method]) {
        result(self.fingerManager.getToken);
    } else {
        result(FlutterMethodNotImplemented);
    }
    
}

#pragma mark - apns 등록
- (void)registeredForRemoteNotifications:(UIApplication *)application {
#if !TARGET_OS_SIMULATOR
    //푸시 등록
    if (@available(iOS 10.0, *)) {

        UNUserNotificationCenter *userNotificationCenter = [UNUserNotificationCenter currentNotificationCenter];
        [userNotificationCenter setDelegate:self];
        [userNotificationCenter requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            
            FPLog(@"granted :%i / error : %@",granted,error);
            
//            if (granted) {
                //허용
                dispatch_async(dispatch_get_main_queue(), ^{
                    [application registerForRemoteNotifications];
                });
//            }
            
        }];
        
    }
    
#endif
}

#pragma mark - FlutterApplicationLifeCycleDelegate

//- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
//
//    NSDictionary *userInfo = [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
//    FPLog(@"didFinishLaunchingWithOptions : %@",userInfo);
//
//    if (userInfo != nil) {
//        [self callReceivedNotificationHandler:userInfo];
//    }
//
//    return YES;
//}

#pragma mark push notification
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
        
    /*핑거푸시에 기기등록*/
    [self.fingerManager registerUserWithBlock:deviceToken :^(NSString *posts, NSError *error) {

        FPLog(@"토큰 : %@",self.fingerManager.getToken);
        FPLog(@"토큰idx : %@",self.fingerManager.getDeviceIdx);
        FPLog(@"기기등록 posts : %@ / error : %@",posts,error);
        
        if (error) {
//            FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
//            [self.channel invokeMethod:@"FingerPush#setDeviceHandler" arguments:flutterError];
            [self.channel invokeMethod:@"FingerPush#setDeviceHandler" arguments:error.description];
            return;
        }
//        [self.channel invokeMethod:@"FingerPush#setDeviceHandler" arguments:@{@"result":posts}];
        [self.channel invokeMethod:@"FingerPush#setDeviceHandler" arguments:@"성공"];

    }];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    FPLog(@"%@", error);
    
//    FlutterError *flutterError = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld",(long)error.code] message:error.localizedDescription details:error.userInfo];
    [self.channel invokeMethod:@"FingerPush#setDeviceHandler" arguments:error.description];

}

- (BOOL)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [self callReceivedNotificationHandler:userInfo];
    return YES;
}

#pragma mark ios10 Apple Push Notification Handler

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler  API_AVAILABLE(ios(10.0)){
    //
    self.countTryCallWillPresent = 0;
    NSDictionary *userInfo = notification.request.content.userInfo;
    [self callWillPresentNotificationHandler:userInfo];
    //
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionBadge);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler  API_AVAILABLE(ios(10.0)){
    
    FPLog(@"response.actionIdentifier : %@",response.actionIdentifier);
    FPLog(@"userNotificationCenter Userinfo %@",response.notification.request.content.body);
    FPLog(@"userNotificationCenter push data = %@",response.notification.request.content.userInfo);
    
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    
    NSString *strAction = response.actionIdentifier;
    if ([strAction containsString:@"yes"] || [strAction containsString:@"UNNotificationDefaultActionIdentifier"]) {
        [self callReceivedNotificationHandler:userInfo];
    }
    
    completionHandler();
    
}

#pragma mark - willPresentNotificationHandler
- (void)callWillPresentNotificationHandler:(NSDictionary*)userInfo {
    
    NSLog(@"willPresentNotificationHandler userInfo : %@",userInfo);
    NSLog(@"willPresentNotificationHandler channel %@",self.channel);
    
    if (self.countTryCallWillPresent > 30) {
        self.countTryCallWillPresent = 0;
        return;
    }
    
    if (self.channel == nil) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.countTryCallWillPresent++;
            [self callWillPresentNotificationHandler:userInfo];
        });
        return;
    }
    
    [self.channel invokeMethod:@"FingerPush#willPresentNotificationHandler" arguments:[self convertedFromObjToSting:userInfo] result:^(id  _Nullable result) {
        
        NSLog(@"willPresentNotificationHandler result : %@",result);
                
        if (result == nil) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.countTryCallWillPresent++;
                [self callWillPresentNotificationHandler:userInfo];
            });
            return;
        }

        FlutterError *err = (FlutterError*)result;
        NSLog(@"message : %@",err.message);

        if ([[NSString stringWithFormat:@"%@",err.message] isEqualToString:@"null"]) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.countTryCallWillPresent++;
                [self callWillPresentNotificationHandler:userInfo];
            });
        }
                        
    }];

}

#pragma mark - receivedNotificationHandler
- (void)callReceivedNotificationHandler:(NSDictionary*)userInfo {
    
    NSLog(@"receivedNotificationHandler userInfo : %@",userInfo);
    NSLog(@"receivedNotificationHandler channel %@",self.channel);
    
    if (self.channel == nil) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self callReceivedNotificationHandler:userInfo];
        });
        return;
    }
    
    if ([self.fingerManager getAppKey] == nil || [self.fingerManager getSecrete] == nil) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self callReceivedNotificationHandler:userInfo];
        });
        return;
    }
    
    if ([[self.fingerManager getSecrete] isEqualToString:@""] || [[self.fingerManager getSecrete] isEqualToString:@""]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self callReceivedNotificationHandler:userInfo];
        });
        return;
    }

    [self.channel invokeMethod:@"FingerPush#receivedNotificationHandler" arguments:[self convertedFromObjToSting:userInfo] result:^(id  _Nullable result) {
        
        NSLog(@"receivedNotificationHandler result : %@",result);
                
        if (result == nil) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self callReceivedNotificationHandler:userInfo];
            });
            return;
        }
        
        FlutterError *err = (FlutterError*)result;
        NSLog(@"message : %@",err.message);

        if ([[NSString stringWithFormat:@"%@",err.message] isEqualToString:@"null"]) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self callReceivedNotificationHandler:userInfo];
            });
        }

    }];

}

#pragma mark - 인앱푸시


#pragma mark - 기타

- (NSString*)convertedFromObjToSting:(id)obj {
    if (obj == nil) {
        return @"";
    }
    NSData *dataJson = [NSJSONSerialization dataWithJSONObject:obj options:NSJSONWritingPrettyPrinted error:nil];
    return [[NSString alloc]initWithData:dataJson encoding:NSUTF8StringEncoding];
}

@end
