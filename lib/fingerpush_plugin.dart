import 'dart:async';
import 'dart:io';
import 'dart:core';
import 'package:flutter/services.dart';

// iOS Handlers
typedef void SetDeviceHandler(String result);
typedef void ReceivedNotificationHandler(String notificationValue);
typedef void WillPresentNotificationHandler(String notificationValue);
//iOS, Android Handlers
typedef void SetInAppPushHandler(String inAppPushValue);

class FingerPush {

  static FingerPush shared = new FingerPush();
  //
  MethodChannel _channel = const MethodChannel('fingerpush_plugin');
  //Android notification
  MethodChannel _channelNotification = const MethodChannel('FingerPushOnNotification');

  //iOS event handler
  SetDeviceHandler? _onSetDeviceHandler;
  WillPresentNotificationHandler? _onWillPresentNotificationHandler;
  //iOS, Android event handler
  ReceivedNotificationHandler? _onReceivedNotificationHandler;
  SetInAppPushHandler? _onSetInAppPushHandler;

  FingerPush() {
    this._channel.setMethodCallHandler(_handleMethod);
    if (Platform.isAndroid) {
      this._channelNotification.setMethodCallHandler(_handleMethodOnNotification);
      getOnNotification();
    }
  }

  //iOS 기기등록 결과 handler
  void setDeviceHandler(SetDeviceHandler handler) {
    _onSetDeviceHandler = handler;
  }

  //iOS 리모트 푸시 메세지 앱 사용 중 뜰 때 수신 handler
  void willPresentNotificationHandler(WillPresentNotificationHandler handler) {
    _onWillPresentNotificationHandler = handler;
  }

  //iOS,Android 리모트 푸시 메세지 수신 handler
  void receivedNotificationHandler(ReceivedNotificationHandler handler) {
    _onReceivedNotificationHandler = handler;
  }

  //iOS,Android InAppPush handler
  void setInAppPushHandler(SetInAppPushHandler handler) {
    _onSetInAppPushHandler = handler;
  }

  Future<String> getPlatformVersion() async {

    if (Platform.isIOS){
      return "";
    } else if (Platform.isAndroid) {
      final String version = await _channel.invokeMethod("getPlatformVersion");
      return version;
    }

    return "";
  }

  //Android OnNotification
  //푸시로 앱 시작시 사용
  Future<void> getOnNotification() async {
    await _channelNotification.invokeMethod("onNotification");
  }

  //핑거푸시에서 보낸 푸시 메지시인지 확인
  //핑거푸시로만 발송하실 경우에는
  static bool isFingerPushPayLoad(Map<dynamic, dynamic> notificationValueMap) {

    String src = "";
    if (Platform.isIOS) {
      src = notificationValueMap["src"];
    } else if (Platform.isAndroid) {
      src = notificationValueMap["data.src"];
    }

    if (src == "fp") {
      //핑거푸시로부터 받은 푸시메시지
      return true;
    }

    return false;
  }



  //앱아이디 저장
  Future<void> setAppKey(String appKey) async {
    // try {
    //   if (Platform.isIOS){
    //     await _channel.invokeMethod("FingerPush#setAppKey", appKey);
    //   } else if (Platform.isAndroid) {
    //     await _channel.invokeMethod('setAppKey', {'appKey': appKey,});
    //   }
    // } on PlatformException catch(e) {
    //   return Future.error(e);
    // }
      if (Platform.isIOS){
        await _channel.invokeMethod("FingerPush#setAppKey", appKey);
      } else if (Platform.isAndroid) {
        await _channel.invokeMethod('setAppKey', {'appKey': appKey,});
      }
  }

  //앱 시크리트 키 저장
  Future<void> setAppSecret(String appSecret) async {
    // try {
    //   if (Platform.isIOS){
    //     await _channel.invokeMethod("FingerPush#setAppSecret", appSecret);
    //   } else if (Platform.isAndroid) {
    //     await _channel.invokeMethod('setAppSecret', {'appSecret': appSecret,});
    //   }
    // } on PlatformException catch(e) {
    //   return Future.error(e);
    // }
      if (Platform.isIOS){
        await _channel.invokeMethod("FingerPush#setAppSecret", appSecret);
      } else if (Platform.isAndroid) {
        await _channel.invokeMethod('setAppSecret', {'appSecret': appSecret,});
      }
  }

  //핑거푸시서버에 기기등록
  //iOS
  Future<void> setDeviceForIOS() async {

    if (Platform.isIOS){
      await _channel.invokeMethod("FingerPush#setDevice");
    }

  }
  //Android
  Future<String> get setDevice async {

    if (Platform.isAndroid) {
      try {
        final String result = await _channel.invokeMethod('setDevice');
        return result;
      } on PlatformException catch(e) {
        return Future.error(e);
      }
    }

    return "";
  }

  //푸시 활성화 on-off
  //디바이스 토큰이 등록된 이후에 호출해 주십시오
  Future<String> setPushAlive(bool isAlive) async {
    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#setEnable", isAlive);
        return response;
      } else if (Platform.isAndroid) {
        final String result =
        await _channel.invokeMethod('setPushAlive', {'isAlive': isAlive,});
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //광고 푸시 수신 여부 처리
  //디바이스 토큰이 등록된 이후에 호출해 주십시오
  Future<String> setAdvertisePushEnable(bool isEnable) async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#setAdPushEnable", isEnable);
        return response;
      } else if (Platform.isAndroid) {
        final String result =
        await _channel.invokeMethod('setAdvertisePushEnable', {'isAlive': isEnable,});
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //핑거푸시서버에서 보내진 푸시내용 요청
  Future<String> getPushContent(Map<dynamic, dynamic> notificationValue) async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestPushContent", notificationValue);
        return response;
      } else if (Platform.isAndroid) {

        String tag = notificationValue['data.msgTag'];
        String mode = notificationValue['data.code'];

        final String result = await _channel.invokeMethod('getPushContent', <String, String>{'tag': tag,'mode': mode,});
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //기기 설정정보 요청
  Future<String> getDeviceInfo() async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestPushInfo");
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('getDeviceInfo');
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //Tag 등록
  //디바이스 토큰이 등록된 이후에 호출해 주십시오
  Future<String> setTag(String tag) async {

    try {
      if (Platform.isIOS) {
        List<String> tagsList = tag.split(',');
        String response = await _channel.invokeMethod("FingerPush#requestRegTag",tagsList);
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('setTag', {'tag': tag,});
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //Tag 삭제
  Future<String> removeTag(String tag) async {

    try {
      if (Platform.isIOS){
        List<String> tagsList = tag.split(',');
        String response = await _channel.invokeMethod("FingerPush#requestRemoveTag",tagsList);
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('removeTag', {'tag': tag,});
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //모든 Tag 삭제
  Future<String> removeAllTag() async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestRemoveAllTag");
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('removeAllTag');
        return result;
      } else {
        return "";
      }

    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //기기에 등록된 태그 리스트조회
  //디바이스 토큰이 등록된 이후에 호출해 주십시오
  Future<String> getTag() async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestGetDeviceTagList");
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('getDeviceTag');
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  // 앱의 모든 태그 리스트조회
  Future<String> getAllTag() async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestGetAllTagList");
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('getAllTag');
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //식별자 등록
  //디바이스 토큰이 등록된 이후에 호출해 주십시오
  Future<String> setIdentity(String identity) async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestRegId",identity);
        return response;
      } else if (Platform.isAndroid) {
        String result = await _channel.invokeMethod('setIdentity', {'identity': identity,});
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }
  }

  //식별자 제거
  Future<String> removeIdentity() async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestRemoveId");
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('removeIdentity');
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //유니크 식별자 등록
  //디바이스 토큰이 등록된 이후에 호출해 주십시오
  Future<String> setUniqueIdentity(String identity, bool isAlram, String msg ) async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestRegUniqId",{'UniqId':identity,'IsAlram':isAlram,'Msg':msg});
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('setUniqueIdentity', {
          'identity': identity,
          'isReceiveMessage': isAlram,
          'message': msg
        });
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //앱 정보 요청
  Future<String> getAppReport() async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestGetAppReport");
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('getAppReport');
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //메세지 수신 확인
  Future<String> checkPush(Map<dynamic, dynamic> notificationValue) async {

    try {

      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestPushCheck", notificationValue);
        return response;
      } else if (Platform.isAndroid) {
        String result = await _channel.invokeMethod('checkPush', notificationValue);
        return result;
      } else {
        return "";
      }

    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //푸시 리스트
  Future<String> getPushList() async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestPushList");
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('getPushList');
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //푸시 리스트 페이지
  Future<String> getPushListPage(int page, int listcnt) async {

    try {
      if (Platform.isIOS){
        String response = await _channel.invokeMethod("FingerPush#requestPushListPage", {'page': page,'listcnt': listcnt});
        return response;
      } else if (Platform.isAndroid) {
        final String result = await _channel.invokeMethod('getPushListPage', {'page': page,'listcnt': listcnt,});
        return result;
      } else {
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }

  //인앱 푸시 띄우기
  Future<void> showInAppPush() async {
    try {
      if (Platform.isIOS){
        await _channel.invokeMethod("FingerPush#showInAppPush");
      } else if (Platform.isAndroid) {
        await _channel.invokeMethod("showInAppPush");
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }
  }

  //디바이스 토큰
  Future<String> getToken() async {

    try {
      if (Platform.isIOS){
        String token = await _channel.invokeMethod("FingerPush#getToken");
        return token;
      } else if (Platform.isAndroid) {
        String token = await _channel.invokeMethod('getToken');
        return token;
      } else{
        return "";
      }
    } on PlatformException catch(e) {
      return Future.error(e);
    }

  }


  //iOS
  Future<Null> _handleMethod(MethodCall call) async {

    if (Platform.isIOS){

      if(call.method == 'FingerPush#setDeviceHandler'){

        if(this._onSetDeviceHandler == null){
          print("setDeviceHandler null");
        }else{
          // 기기 등록 결과
          this._onSetDeviceHandler!(call.arguments.toString());
        }

      }else if(call.method == 'FingerPush#receivedNotificationHandler'){

        if (this._onReceivedNotificationHandler == null){
          print("_onReceivedNotificationHandler null");
          return Future.error("null");
        }else{
          // 리모트 푸시 메시지 데이터
          this._onReceivedNotificationHandler!(call.arguments.toString());
          return Future.error("success");
        }

      }else if(call.method == 'FingerPush#willPresentNotificationHandler'){

        if (this._onWillPresentNotificationHandler == null){
          print("_onWillPresentNotificationHandler null");
          return Future.error("null");
        }else{
          // 리모트 푸시 메시지 데이터
          this._onWillPresentNotificationHandler!(call.arguments.toString());
          return Future.error("success");
        }

      }else if(call.method == 'FingerPush#setInAppPushHandler'){

        if (this._onSetInAppPushHandler == null){
          print("_onSetInAppPushHandler null");
          return Future.error("null");
        }else{
          // 인앱푸시 데이터
          this._onSetInAppPushHandler!(call.arguments.toString());
          return Future.error("success");
        }

      }

    } else if (Platform.isAndroid) {

      if(call.method == 'showInAppPush'){

        if (this._onSetInAppPushHandler == null){
          print("_onSetInAppPushHandler null");
          return Future.error("null");
        }else{
          // 인앱푸시 데이터
          this._onSetInAppPushHandler!(call.arguments.toString());
          return Future.error("success");
        }
        
      }

    }

    return null;
  }

  //Android
  Future<Null> _handleMethodOnNotification(MethodCall call) async {

    if(call.method == 'onNotification'){

      if(this._onReceivedNotificationHandler == null){
        print("_onReceivedNotificationHandler null");
      }else{
        this._onReceivedNotificationHandler!(call.arguments.toString());
      }

    }

    return null;
  }

}