package com.fingerpush.fingerpush_plugin

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.annotation.NonNull
import com.fingerpush.android.FingerPushManager
import com.fingerpush.android.NetworkUtility
import com.fingerpush.android.interfaces.onCampaignClickListener
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import org.json.JSONObject


/** FingerpushPlugin */
public class FingerpushPlugin : FlutterPlugin, ActivityAware, MethodCallHandler {

    private lateinit var channel: MethodChannel
    private lateinit var context: Context
    private lateinit var activity: Activity
    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val channel = MethodChannel(registrar.messenger(), "fingerpush_plugin")
            channel.setMethodCallHandler(FingerpushPlugin())
        }
    }

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "fingerpush_plugin")
        channel.setMethodCallHandler(this);
        context = flutterPluginBinding.applicationContext
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding.activity
    }

    override fun onDetachedFromActivityForConfigChanges() {}

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {}

    override fun onDetachedFromActivity() {}

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        if (call.method == "setAppKey") {
            FingerPushManager.setAppKey(call.argument("appKey"));
            return
        }

        if (call.method == "setAppSecret") {
            FingerPushManager.setAppSecret(call.argument("appSecret"));
            return
        }

        if (call.method == "setDevice") {
            FingerPushManager.getInstance(context)
                .setDevice(object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "checkPush") {
            val map: HashMap<String, String> = call.arguments as HashMap<String, String>
            val bundle = Bundle()

            for (key in map.keys) {
                bundle.putString(key, map.get(key))
            }

            FingerPushManager.getInstance(context)
                .checkPush(bundle, object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })
            return
        }

        if (call.method == "getAllTag") {
            FingerPushManager.getInstance(context)
                .getAllTag(object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        data?.put("code", code)
                        data?.put("message", message)
                        result.success(data.toString())
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "getAppReport") {
            FingerPushManager.getInstance(context)
                .getAppReport(object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        data?.put("code", code)
                        data?.put("message", message)
                        result.success(data.toString())
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "getDeviceInfo") {
            FingerPushManager.getInstance(context)
                .getDeviceInfo(object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        data?.put("code", code)
                        data?.put("message", message)
                        result.success(data.toString())
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "getDeviceTag") {
            FingerPushManager.getInstance(context)
                .getDeviceTag(object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        data?.put("code", code)
                        data?.put("message", message)
                        result.success(data.toString())
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "getPushContent") {
            FingerPushManager.getInstance(context).getPushContent(
                call.argument("tag"),
                call.argument("mode"),
                object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        data?.put("code", code)
                        data?.put("message", message)
                        result.success(data.toString())
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "getPushList") {
            FingerPushManager.getInstance(context)
                .getPushList(object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        data?.put("code", code)
                        data?.put("message", message)
                        result.success(data.toString())
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "getPushListPage") {
            FingerPushManager.getInstance(context).getPushListPage(
                call.argument<Int>("page")!!,
                call.argument<Int>("listcnt")!!,
                object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        data?.put("code", code)
                        data?.put("message", message)
                        result.success(data.toString())
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "removeIdentity") {
            FingerPushManager.getInstance(context)
                .removeIdentity(object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?)
                    {
                        result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "removeTag") {
            FingerPushManager.getInstance(context)
                .removeTag(call.argument<String>("tag")!!, object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "removeAllTag") {
            FingerPushManager.getInstance(context)
                .removeAllTag(object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "setIdentity") {
            FingerPushManager.getInstance(context)
                .setIdentity(call.argument("identity"), object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "setUniqueIdentity") {
            FingerPushManager.getInstance(context)
                .setUniqueIdentity(
                    call.argument("identity"),
                    call.argument<Boolean>("isReceiveMessage")!!,
                    call.argument("message"),
                    object : NetworkUtility.ObjectListener {
                        override fun onComplete(
                            code: String?,
                            message: String?,
                            data: JSONObject?
                        ) {
                            result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                        }

                        override fun onError(code: String?, message: String?) {
                            result.error(code!!, message, null)
                        }
                    })

            return
        }

        if (call.method == "setPushAlive") {
            FingerPushManager.getInstance(context).setPushAlive(
                call.argument<Boolean>("isAlive")!!,
                object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "setPushEnable") {
            FingerPushManager.getInstance(context).setPushEnable(
                call.argument<Boolean>("isAlive")!!,
                object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "setAdvertisePushEnable") {
            FingerPushManager.getInstance(context).setAdvertisePushEnable(
                call.argument<Boolean>("isAlive")!!,
                object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "setTag") {
            FingerPushManager.getInstance(context)
                .setTag(call.argument("tag")!!, object : NetworkUtility.ObjectListener {
                    override fun onComplete(code: String?, message: String?, data: JSONObject?) {
                        result.success("{\"code\":\"$code\", \"message\":\"$message\"}")
                    }

                    override fun onError(code: String?, message: String?) {
                        result.error(code!!, message, null)
                    }
                })

            return
        }

        if (call.method == "showInAppPush") {
            FingerPushManager.getInstance(activity as Context)
                .showInAppPush(object : onCampaignClickListener {
                    override fun onClick(data: JSONObject?) {
                        val arguments = HashMap<String, String>()
                        arguments["event"] = "Click"
                        arguments["data"] = data.toString()
                        channel.invokeMethod("showInAppPush", arguments)
                    }

                    override fun onFailed(code: String?, message: String?) {
                        val arguments = HashMap<String, String>()
                        arguments["event"] = "Failed"
                        arguments["data"] = hashMapOf(
                            "code" to code,
                            "message" to message
                        ).toString()
                        channel.invokeMethod("showInAppPush", arguments)
                    }

                    override fun onNotToday(data: JSONObject?) {
                        val arguments = HashMap<String, String>()
                        arguments["event"] = "NotToday"
                        arguments["data"] = data.toString()
                        channel.invokeMethod("showInAppPush", arguments)
                    }

                    override fun onClose(data: JSONObject?) {
                        val arguments = HashMap<String, String>()
                        arguments["event"] = "Close"
                        arguments["data"] = data.toString()
                        channel.invokeMethod("showInAppPush", arguments)
                    }
                })
            return
        }

        if (call.method == "getToken") {
            result.success(FingerPushManager.getToken(context))
            return
        }

        result.notImplemented()
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }
}