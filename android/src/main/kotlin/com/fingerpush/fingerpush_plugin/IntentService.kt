package com.fingerpush.fingerpush_plugin

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import com.fingerpush.android.FingerNotification
import com.fingerpush.android.FingerPushFcmListener

class IntentService : FingerPushFcmListener() {
    override fun onMessage(context: Context?, bundle: Bundle?) {
        val packageManager: PackageManager = getPackageManager()
        val intent: Intent = packageManager.getLaunchIntentForPackage(packageName)!!
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        intent.putExtra("payload", bundle)

        val pendingIntent : PendingIntent
        pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.getActivity(
                this,
                System.currentTimeMillis().toInt(),
                intent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
            )
        } else {
            PendingIntent.getActivity(
                this,
                System.currentTimeMillis().toInt(), intent, PendingIntent.FLAG_CANCEL_CURRENT
            )
        }

        val fingerNotification = FingerNotification(this@IntentService)
        fingerNotification.setNotificationIdentifier(System.currentTimeMillis().toInt())
        fingerNotification.setIcon(R.drawable.ic_stat_name);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fingerNotification.setColor(Color.parseColor("#00cfb5"));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fingerNotification.createChannel("channel_id", "알림");
        }

        fingerNotification.setVibrate(longArrayOf(0, 500, 600, 1000))
        fingerNotification.setLights(Color.parseColor("#ffff00ff"), 500, 500);
        fingerNotification.showNotification(bundle!!, pendingIntent);
    }
}