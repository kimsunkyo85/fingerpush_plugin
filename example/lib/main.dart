import 'package:flutter/material.dart';
import 'package:fingerpush_plugin/fingerpush_plugin.dart';
import 'dart:io';
import 'dart:convert';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(title: Text('FingerPushExample')),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: BodyLayout(),
          )),
    );
  }

}

class BodyLayout extends StatefulWidget {

  @override
  MyListView createState() {
    return MyListView();
  }

}

class MyListView extends State<BodyLayout> {

  //핑거푸시 호출 결과
  //기기등록
  String strRegisterDeviceResult = '기기 토큰이 등록 후 결과 메시지가 여기에 표시됩니다.';
  //푸시 내용
  String strPushContentResult = '푸시 메시지가 수신되면 여기에 나타납니다.';
  //푸시 체크
  String strPushCheckResult = '푸시 메시지가 수신되면 여기에 나타납니다.';
  //태그 등록 설정
  String strRegTagResult = 'Tag를 설정할 수 있으며, 결과 메시지가 여기에 표시됩니다.';
  final TextEditingController regTagTextController = new TextEditingController();
  //태그 지우기
  String strRemoveTagResult = '입력한 Tag를 삭제할 수 있으며, 결과 메시지가 여기에 표시됩니다.';
  final TextEditingController removeTagTextController = new TextEditingController();
  //모든 태그 지우기
  String strRemoveAllTagResult = '모든 Tag를 삭제할 수 있으며, 결과 메시지가 여기에 표시됩니다.';
  //앱의 모든 태그 가져오기
  String strGetAllTagResult = '앱 전체 Tag를 가져와서 여기에 표시합니다.';
  //기기 태그 리스트 가져오기
  String strGetDeviceTagListResult = '디바이스 Tag를 가져와서 여기에 표시합니다.';
  //identity 설정
  String strRegIdResult = 'Identity를 설정할 수 있으며, 결과 메시지가 여기에 표시됩니다.';
  final TextEditingController regIdTextController = new TextEditingController();
  //uniqIdentity 설정
  String strRegUniqIdResult = 'Uniq Identity를 설정할 수 있으며, 결과 메시지가 여기에 표시됩니다.';
  final TextEditingController regUniqIdTextController = new TextEditingController();
  //Identity 삭제
  String strRemoveIdResult = '설정된 Identity를 삭제하며, 결과 메시지가 여기에 표시됩니다.';
  //앱정보 확인
  String strGetAppReportResult = '앱이 설치된 정보가 표시됩니다.';
  //푸시설정 가져오기
  String strPushInfoResult = '디바이스 타입과 푸시 수신여부, identity를 확인할 수 있습니다.';
  //setEnable
  String strSetEnableResult = '푸시 메시지 수신 여부를 설정할 수 있으며, true/false만 가능합니다.';
  //setAdEnable
  String strSetAdEnableResult = '광고 푸시 메시지 수신 여부를 설정할 수 있으며, true/false만 가능합니다.';
  //푸시 리스트 가져오기
  String strGetPushListResult = '푸시리스트를 표시합니다.';
  //인앱푸시
  String strShowInAppPushResult = '인앱푸시를 노출합니다.';
  //토큰
  String strGetTokenResult = '디바이스 토큰 값을 표시합니다.';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initFingerPush();
  }


  //void
  Future<void> initFingerPush() async {
      FingerPush.shared.setAppKey("NV0MIEN56A19");
      FingerPush.shared.setAppSecret("wfe4GQ3c1v8Xnidt1Tf9AfkISlPRqbfi");

      //기기 등록 후 결과
      FingerPush.shared.setDeviceHandler((result) {
        setState(() {
          strRegisterDeviceResult = result;
        });
      });

    // 리모트 푸시 메시지 수신 정보
    // 사용자가 푸시를 탭하였을 경우에만 반응합니다.
    FingerPush.shared.receivedNotificationHandler((notificationValue) {

      print(notificationValue);

      Map notificationValueMap = json.decode(notificationValue);

      //푸시 페이로드가 핑거푸시인지 확인
      bool isFingerPushNotication = FingerPush.isFingerPushPayLoad(notificationValueMap);
      if (isFingerPushNotication == false) {
        return;
      }

      //리모트 푸시 읽음 처리
      FingerPush.shared.checkPush(notificationValueMap).then((value) {
        setState(() {
          strPushCheckResult = value;
        });
      }).catchError((onError){
        setState(() {
          strPushCheckResult = onError.toString();
        });
      });

      //푸시 페이로드드에 포함되어 않은 리모트 푸시 정보
      //일반적으로는 사용하지 않아도 무방
      FingerPush.shared.getPushContent(notificationValueMap).then((value) {
        setState(() {
          strPushContentResult = value;
        });
      }).catchError((onError){
        setState(() {
          strPushContentResult = onError.toString();
        });
      });

    });

    registerDevice();

  }

  //기기등록
  void registerDevice() {

    if (Platform.isIOS){

      FingerPush.shared.setDeviceForIOS();

    } else if (Platform.isAndroid) {

      FingerPush.shared.setDevice.then((value) {
        setState(() {
          strRegisterDeviceResult = value;
        });
      }).catchError((e) {
        setState(() {
          strRegisterDeviceResult = e.toString();
        });
      });

    }

  }

  //태그 등록 설정
  void regTag(String str) {
    FingerPush.shared.setTag(str).then((value) {
      setState(() {
        strRegTagResult = value;
      });
    }).catchError((onError){
      setState(() {
        strRegTagResult = onError.toString();
      });
    });
  }

  //태그 지우기
  void removeTag(String str) {
    FingerPush.shared.removeTag(str).then((value) {
      setState(() {
          strRemoveTagResult = value;
      });
    }).catchError((onError){
      setState(() {
          strRemoveTagResult = onError.toString();
      });
    });
  }

  //모든 태그 지우기
  void removeAllTag() {
    FingerPush.shared.removeAllTag().then((value) {
      setState(() {
          strRemoveAllTagResult = value;
      });
    }).catchError((onError){
      setState(() {
          strRemoveAllTagResult = onError.toString();
      });
    });
  }

  //앱의 모든 태그 가져오기
  void getAllTag() {
    FingerPush.shared.getAllTag().then((value) {
      setState(() {
          strGetAllTagResult = value;
      });
    }).catchError((onError){
      setState(() {
          strGetAllTagResult = onError.toString();
      });
    });
  }

  //기기 태그 리스트 가져오기
  void getDeviceTagList() {
    FingerPush.shared.getTag().then((value) {
      setState(() {
          strGetDeviceTagListResult = value;
      });
    }).catchError((onError){
      setState(() {
          strGetDeviceTagListResult = onError.toString();
      });
    });
  }

  //identity 설정
  void regId(String str) {
    FingerPush.shared.setIdentity(str).then((value) {
      setState(() {
          strRegIdResult = value;
      });
    }).catchError((onError){
      setState(() {
          strRegIdResult = onError.toString();
      });
    });
  }

  //uniqIdentity 설정
  void regUniqId(String str) {
    FingerPush.shared.setUniqueIdentity(str,true,"").then((value) {
      setState(() {
          strRegUniqIdResult = value;
      });
    }).catchError((onError){
      setState(() {
          strRegUniqIdResult = onError.toString();
      });
    });
  }

  //Identity 삭제
  void removeId() {
    FingerPush.shared.removeIdentity().then((value) {
      setState(() {
          strRemoveIdResult = value;
      });
    }).catchError((onError){
      setState(() {
          strRemoveIdResult = onError.toString();
      });
    });
  }

  //앱정보 확인
  void getAppReport() {
    FingerPush.shared.getAppReport().then((value) {
      setState(() {
          strGetAppReportResult = value;
      });
    }).catchError((onError){
      setState(() {
          strGetAppReportResult = onError.toString();
      });
    });
  }

  //푸시설정 가져오기
  void getPushInfo() {
    FingerPush.shared.getDeviceInfo().then((value) {
      setState(() {
          strPushInfoResult = value;
      });
    }).catchError((onError){
      setState(() {
          strPushInfoResult = onError.toString();
      });
    });
  }

  //setEnable
  void setEnableOn() {
    FingerPush.shared.setPushAlive(true).then((value) {
      setState(() {
          strSetEnableResult = value;
      });
    }).catchError((onError){
      setState(() {
          strSetEnableResult = onError.toString();
      });
    });
  }

  void setEnableOff() {
    FingerPush.shared.setPushAlive(false).then((value) {
      setState(() {
          strSetEnableResult = value;
      });
    }).catchError((onError){
      setState(() {
          strSetEnableResult = onError.toString();
      });
    });
  }

  //setAdEnable
  void setAdEnableOn() {
    FingerPush.shared.setAdvertisePushEnable(true).then((value) {
      setState(() {
          strSetAdEnableResult = value;
      });
    }).catchError((onError){
      setState(() {
          strSetAdEnableResult = onError.toString();
      });
    });
  }

  void setAdEnableOff() {
    FingerPush.shared.setAdvertisePushEnable(false).then((value) {
      setState(() {
          strSetAdEnableResult = value;
      });
    }).catchError((onError){
      setState(() {
          strSetAdEnableResult = onError.toString();
      });
    });
  }

  //푸시 리스트 가져오기
  void getPushList() {
    FingerPush.shared.getPushList().then((value) {
      setState(() {
          strGetPushListResult = value;
      });
    }).catchError((onError) {
      setState(() {
          strGetPushListResult = onError.toString();
      });
    });
  }

  //인앱 푸시 띄우기, 띄우기 결과, 사용자 액션 결과
  void showInAppPush() {
    FingerPush.shared.showInAppPush();
    FingerPush.shared.setInAppPushHandler((result) {
      setState(() {
        strShowInAppPushResult = result;
      });
    });
  }

  //디바이스 토큰 가져오기
  void getToken() {
    FingerPush.shared.getToken().then((value) {
      setState(() {
        strGetTokenResult = value;
      });
    }).catchError((onError) {
      setState(() {
        strGetTokenResult = onError.toString();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: ListTile.divideTiles(
        context: context,
        tiles: [
          ListTile(
            //leading. 타일 앞에 표시되는 위젯. 참고로 타일 뒤에는 trailing 위젯으로 사용 가능
            title: Text('setDevice'),
            subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('기기 등록하기'),
                      onPressed: () {
                          registerDevice();
                      }),
                  Container(
                    padding: EdgeInsets.only(top: 5),
                    child: Text(strRegisterDeviceResult),
                  )
                ],
              )),
          //PushContent
          ListTile(
            title: Text('PushContent'),
            subtitle: new Text(strPushContentResult),
            // Text('success/error\n*content: \n*date:')
          ),

          //PushCheck
          ListTile(title: new Text('PushCheck'),
              subtitle: Text(strPushCheckResult)
              // Text('success/error')
              ),

          //RegTag
          ListTile(
            title: Text('RegTag'),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 50,
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      RaisedButton(
                          onPressed: () {
                            regTag(regTagTextController.text);
                            regTagTextController.clear();
                          },
                          child: Text("Tag 설정")),
                      Expanded(
                          child: TextField(
                        controller: regTagTextController,
                      ))
                    ],
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(top: 5), child: Text(strRegTagResult))
              ],
            ),
          ),

          //RemoveTag
          ListTile(
            title: Text('RemoveTag'),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 50,
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      RaisedButton(
                          onPressed: () {
                            removeTag(removeTagTextController.text);
                            removeTagTextController.clear();
                          },
                          child: Text("Tag 지우기")),
                      Expanded(
                          child: TextField(
                        controller: removeTagTextController,
                      ))
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 5),
                  child: new Text(strRemoveTagResult),
                )
              ],
            ),
          ),

          //removeAllTag
          ListTile(
              title: Text('removeAllTag'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('모든 Tag 지우기'),
                      onPressed: () {
                        removeAllTag();
                      }),
                  Container(
                    padding: EdgeInsets.only(top: 5),
                    child: new Text(strRemoveAllTagResult),
                  )
                ],
              )),

          //GetAllTagList
          ListTile(
              title: Text('getAllTagList'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('Tag 가져오기'),
                      onPressed: () {
                        getAllTag();
                      }),
                  Container(
                      padding: EdgeInsets.only(top: 5),
                      child: new Text(strGetAllTagResult))
                ],
              )),

          //GetDeviceTagList
          ListTile(
              title: Text('getDeviceTagList'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('Tag 가져오기'),
                      onPressed: () {
                        getDeviceTagList();
                      }),
                  Container(
                      padding: EdgeInsets.only(top: 5),
                      child: new Text(strGetDeviceTagListResult))
                ],
              )),

          //RegId
          ListTile(
            title: Text('RegId'),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 50,
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      RaisedButton(
                          onPressed: () {
                            regId(regIdTextController.text);
                            regIdTextController.clear();
                          },
                          child: Text("Identity 설정")),
                      Expanded(
                          child: TextField(
                        controller: regIdTextController,
                      ))
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 5),
                  child: new Text(strRegIdResult),
                )
              ],
            ),
          ),

          //RegUniqId
          ListTile(
            title: Text('RegUniqId'),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 50,
                  child: Row(
                    textDirection: TextDirection.rtl,
                    children: <Widget>[
                      RaisedButton(
                          onPressed: () {
                            regUniqId(regUniqIdTextController.text);
                            regUniqIdTextController.clear();
                          },
                          child: Text("Uniq Identity 설정")),
                      Expanded(
                          child: TextField(
                        controller: regUniqIdTextController,
                      ))
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 5),
                  child: new Text(strRegUniqIdResult),
                )
              ],
            ),
          ),

          //RemoveId
          ListTile(
              title: Text('RemoveId'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('Identity 삭제'),
                      onPressed: () {
                        removeId();
                      }),
                  Container(
                    padding: EdgeInsets.only(top: 5),
                    child: new Text(strRemoveIdResult),
                  )
                ],
              )),

          //GetAppReport
          ListTile(
              title: Text('GetAppReport'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('앱정보 확인'),
                      onPressed: () {
                        getAppReport();
                      }),
                  Container(
                      padding: EdgeInsets.only(top: 5),
                      child: new Text(strGetAppReportResult))
                ],
              )),

          //PushInfo
          ListTile(
              title: Text('getPushInfo'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('푸시 설정 가져오기'),
                      onPressed: () {
                        getPushInfo();
                      }),
                  Container(
                    padding: EdgeInsets.only(top: 5),
                    child: new Text(strPushInfoResult),
                  )
                ],
              )),

          //setEnable
          ListTile(
              title: Text('setEnable'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('푸시 ON'),
                      onPressed: () {
                        setEnableOn();
                      }),
                  RaisedButton(
                      child: Text('푸시 OFF'),
                      onPressed: () {
                        setEnableOff();
                      }),
                  Container(
                    padding: EdgeInsets.only(top: 5),
                    child: Text(strSetEnableResult),
                  )
                ],
              )),

          //setAdEnable
          ListTile(
              title: Text('setAdEnable'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('푸시 ON'),
                      onPressed: () {
                        setAdEnableOn();
                      }),
                  RaisedButton(
                      child: Text('푸시 OFF'),
                      onPressed: () {
                        setAdEnableOff();
                      }),
                  Container(
                    padding: EdgeInsets.only(top: 5),
                    child: Text(strSetAdEnableResult),
                  )
                ],
              )),

          //GetPushList
          ListTile(
              title: Text('GetPushList'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('푸시 리스트 가져오기'),
                      onPressed: () {
                        getPushList();
                      }),
                  Container(
                      padding: EdgeInsets.only(top: 5),
                      child: new Text(strGetPushListResult))
                ],
              )),
          //ShowInAppPush
          ListTile(
              title: Text('ShowInAppPush'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('인앱푸시 띄우기'),
                      onPressed: () {
                        showInAppPush();
                      }),
                  Container(
                      padding: EdgeInsets.only(top: 5),
                      child: new Text(strShowInAppPushResult))
                ],
              )),
          //getToken
          ListTile(
              title: Text('getToken'),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                      child: Text('디바이스 토큰'),
                      onPressed: () {
                        getToken();
                      }),
                  Container(
                      padding: EdgeInsets.only(top: 5),
                      child: new Text(strGetTokenResult))
                ],
              )),
        ],
      ).toList(),
    );
  }
}

// void _handleSubmitted(String text) {
//   _textController.clear();
// }
