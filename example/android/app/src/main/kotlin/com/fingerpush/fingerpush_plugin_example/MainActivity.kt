package com.fingerpush.fingerpush_plugin_example

import android.content.Intent
import android.os.Bundle
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugins.GeneratedPluginRegistrant
import org.json.JSONObject


class MainActivity : FlutterFragmentActivity(), MethodCallHandler {

    private lateinit var channel: MethodChannel

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        checkPush(intent.extras)
    }

    // 푸시 읽음 처리
    fun checkPush(data: Bundle?) {
        if (data != null) {
            val jsonObject = JSONObject()
            val payload = data.getBundle("payload")

            val keys = payload?.keySet()
            if (keys != null) {
                for (key in keys) {
                    jsonObject.put(key, payload?.getString(key))
                }

                channel.invokeMethod("onNotification", jsonObject.toString())
            }
        }
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        channel = MethodChannel(flutterEngine.dartExecutor, "FingerPushOnNotification")
        channel.setMethodCallHandler(this@MainActivity)
    }

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        if (call.method.equals("onNotification")) {
            checkPush(intent.extras)
        }
    }
}
